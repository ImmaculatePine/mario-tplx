# Mario Tooploox Edition

## Installation

    npm install
    npm run nodemon

Visit http://localhost:5000

## Audio Sprites

All audio effects are merged into a single audio sprite using `audiosprite` package.

Run this code to update audio sprite:

    npm run audiosprite

It will generate new `frontend/sounds/effects.json` file and `static/sounds/effects.*` sprites.
