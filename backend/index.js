const webpack = require('webpack')
const webpackConfig = require('../webpack.config')
const webpackDevMiddleware = require('webpack-dev-middleware')

const bodyParser = require('body-parser')

const app = require('express')()
const http = require('http').Server(app)
const io = require('socket.io')(http)

const multiplayer = require('./multiplayer')

const PORT = 5000

// Set the view engine to ejs
app.set('view engine', 'ejs')

const compiler = webpack(webpackConfig)
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }))
app.use(bodyParser.urlencoded({ extended: true }))

const LoginService = require('./services/login')
const CountdownService = require('./services/countdown')

app.get('/', (req, res) => {
  if (CountdownService.ok()) {
    res.render('login')
  } else {
    res.render('countdown', { timeout: CountdownService.timeout() })
  }
})

app.post('/', (req, res) => {
  if (CountdownService.ok()) {
    const { username, password } = req.body
    const user = LoginService.login(username, password)
    if (user) {
      res.render('game', {
        username: user.username,
        animationOffset: user.animationOffset,
        shortname: user.shortname
      })
    } else {
      res.render('login')
    }
  } else {
    res.render('countdown', { timeout: CountdownService.timeout() })
  }
})

// Endpoints for testing purposes
app.get('/no-women-allowed', (req, res) => {
  res.render('login')
})

app.post('/no-women-allowed', (req, res) => {
  const { username, password } = req.body
  const user = LoginService.login(username, password)
  if (user) {
    res.render('game', {
      username: user.username,
      animationOffset: user.animationOffset,
      shortname: user.shortname
    })
  } else {
    res.render('login')
  }
})

app.get('/no-women-allowed/random', (req, res) => {
  const user = LoginService.loginAsRandomUser()
  res.render('game', {
    username: user.username,
    animationOffset: user.animationOffset,
    shortname: user.shortname
  })
})

multiplayer(io)

http.listen(PORT, error => {
  if (error) {
    console.error(error)
  } else {
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', PORT, PORT)
  }
})
