const uuidv4 = require('uuid/v4')
const time = require('../time')
const USERS = require('../services/users')

const DEFAULT_ROOM = 'level-1-1'
const FINAL_ROOM = 'level-1-2'
const PLAYERS = {}

module.exports = io => {
  io.on('connection', (socket) => {
    const uuid = uuidv4()
    console.log(`${time()}: Player ${uuid} connected`)

    const player = {
      uuid,
      room: null,
      username: null,
      x: 0,
      y: 0
    }

    // Subscribe to socket events
    socket.on('disconnect', () => {
      console.log(`${player.username || uuid} disconnected`)
      socket.broadcast.emit('playerLeft', { uuid })
      if (PLAYERS[uuid]) delete PLAYERS[uuid]
    })

    // Subscribe to other events only after authentication
    socket.on('authenticationRequested', ({ username, x, y }) => {
      console.log(`Player ${uuid} authenticated as ${username}`)

      // Load more info about user
      const userData = USERS.find(u => u.username == username)
      if (!userData) return
      player.x = x
      player.y = y
      player.time = time()
      player.username = username
      player.shortname = userData.shortname
      player.ghostAnimationOffset = userData.ghostAnimationOffset

      // Add player to the list of all players
      PLAYERS[uuid] = player

      const onJoinCallback = room => {
        console.log(`${player.username} joined ${room}`)

        // Update user's room
        player.room = room

        // Inform user that he is successfully authenticated
        // and send to the joined player name of the room and a list of other players
        const otherPlayers = Object
          .values(PLAYERS)
          .filter(p => p.room == room)
          .filter(p => p.uuid != uuid)
        socket.emit('authenticationAccepted', {
          uuid,
          room,
          players: otherPlayers
        })
        console.log(`Other players in this room are`)
        console.log(otherPlayers)

        // Notify other players about newly joined player
        socket.to(room).emit('playerJoined', player)
      }

      const onLeaveCallback = room => {
        // Notify other players about user who left
        socket.to(room).emit('playerLeft', { uuid })
      }

      // Join to the room (one for everyone for now)
      socket.join(DEFAULT_ROOM, () => onJoinCallback(DEFAULT_ROOM))

      socket.on('levelFinished', () => {
        socket.leave(DEFAULT_ROOM, () => {
          onLeaveCallback(DEFAULT_ROOM)
          socket.join(FINAL_ROOM, () => onJoinCallback(FINAL_ROOM))
        })
      })
    })

    socket.on('positionChanged', ({ username, x, y }) => {
      if (!player.room) return
      player.x = x
      player.y = y
      player.time = time()
      socket.to(player.room).emit('positionChanged', { uuid, time: player.time, x, y })
    })
  })
}
