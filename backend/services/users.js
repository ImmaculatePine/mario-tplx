const USERS = [
  {
    username: 'mario',
    password: 'qwerty123',
    shortname: 'Mario'
  },
  {
    username: 'agnieszka.sierzputowska',
    password: 'koty',
    shortname: 'Aga'
  },
  {
    username: 'agnieszka.sitko',
    password: 'trampoliny',
    shortname: 'Aga'
  },
  {
    username: 'anna.langiewicz',
    password: 'ilustracje',
    shortname: 'Anna'
  },
  {
    username: 'barbara.siczek',
    password: 'pluszaki',
    shortname: 'Basia'
  },
  {
    username: 'dobromila.gaca',
    password: 'kocia czapka',
    shortname: 'Milka'
  },
  {
    username: 'dominika.dudek',
    password: 'brokat',
    shortname: 'Dominika'
  },
  {
    username: 'ivona.tautkute',
    password: 'matematyka',
    shortname: 'Ivona'
  },
  {
    username: 'izabela.piotrowska',
    password: 'fitness',
    shortname: 'Iza'
  },
  {
    username: 'joanna.reszewska',
    password: 'fantasy',
    shortname: 'Asia'
  },
  {
    username: 'karolina.polanska',
    password: 'lata 20',
    shortname: 'Karolina'
  },
  {
    username: 'karolina.samorek',
    password: 'kitesurfing',
    shortname: 'Karolina'
  },
  {
    username: 'katarzyna.gorska',
    password: 'wspinaczka',
    shortname: 'Kasia'
  },
  {
    username: 'katarzyna.reps',
    password: 'kawa',
    shortname: 'Kasia'
  },
  {
    username: 'magdalena.tula',
    password: 'alien',
    shortname: 'Magda'
  },
  {
    username: 'maja.rudzinska',
    password: 'arbuzy',
    shortname: 'Maja'
  },
  {
    username: 'martyna.rudzinska',
    password: 'wrotki',
    shortname: 'Martyna'
  },
  {
    username: 'patrycja.duszynska',
    password: 'books and travelling',
    shortname: 'Patrycja'
  },
  {
    username: 'sylwia.tatarczak',
    password: 'motocykle',
    shortname: 'Sylwia'
  },
  {
    username: 'zofia.nowakowska',
    password: 'folklore',
    shortname: 'Zosia'
  }
]

const FRAMES = 14
const GHOST_FRAMES = 3
let i = 0

module.exports = USERS.map(user => {
  const userWithAnimations = Object.assign({
    animationOffset: i * FRAMES,
    ghostAnimationOffset: i * GHOST_FRAMES,
  }, user)
  i++
  return userWithAnimations
})
