const USERS = require('./users')

module.exports = {
  login: (username, password) => {
    return USERS.filter(user => user.username == username && user.password == password)[0]
  },
  loginAsRandomUser: () => {
    return USERS[Math.floor(Math.random() * USERS.length)]
  }
}