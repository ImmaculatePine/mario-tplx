// March 8th, 11:00
const FINAL_DATE = new Date(2018, 2, 8, 10, 0, 0)

module.exports = {
  ok: () => new Date >= FINAL_DATE,
  timeout: () => parseInt((FINAL_DATE - new Date) / 1000)
}
