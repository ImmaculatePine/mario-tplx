// Returns time in milliseconds
module.exports = () => {
  const hrTime = process.hrtime()
  return parseInt(hrTime[0] * 1000 + hrTime[1] / 1000000)
}
