(function() {
  Backbone.Ghost = Backbone.AnimatedTile.extend({
    defaults: {
      name: "ghost",
      type: "artifact",
      width: 32,
      height: 32,
      spriteSheet: "ghost",
      state: "idle",
      collision: false,
      shortname: "Ghost",
      opacity: 0.5
    },
    animations: {
      idle: {
        sequences: [0, 1, 2, 1],
        delay: 50
      }
    },
    onDraw: function(context, options) {
      var shortname = this.get("shortname"),
          x = Math.round(this.get("x") + (options.offsetX || 0) + 16),
          y = Math.round(this.get("y") + (options.offsetY || 0));

      context.fillStyle = "#E0E0E0";
      context.font = "15px arial";
      context.textAlign = "center";
      context.textBaseline = "middle";
      context.fillText(shortname, x, y);

      return this;
    }
  });
  Backbone.Ghost.buildForPlayer = function(player) {
    var animationOffset = player.ghostAnimationOffset,
        x = player.x,
        y = player.y,
        shortname = player.shortname
    var klass = Backbone.Ghost.extend({
      defaults: _.extend(_.deepClone(Backbone.Ghost.prototype.defaults), {
        name: "ghost-" + player.uuid
      }),
      animations: {
        idle: {
          sequences: [0, 1, 2, 1].map(function(value) {
            return value + animationOffset
          }),
          delay: 50
        }
      }
    })
    return new klass({ x: x, y: y, shortname: shortname })
  };
}).call(this);
