const Howler = require('howler')

const EFFECTS = new Howl(require('./effects'))
const MUSIC = {
  'level1-1': new Howl({
    src: ['/static/sounds/level1-1.mp3'],
    loop: true,
  }),
  'level1-2': new Howl({
    src: ['/static/sounds/level1-2.mp3']
  })
}
const MUSIC_VOLUME = 0.4

window.soundManager = {
  enabled: true,
  playingMusic: null,
  playingMusicId: null,
  playEffect: function(name) {
    if (!this.enabled) return
    EFFECTS.play(name)
  },
  playMusic: function(name, volume) {
    if (!this.enabled) return
    const music = MUSIC[name]
    if (volume) {
      music.volume = volume
    }
    this.playingMusic = music
    this.playingMusicId = this.playingMusic.play()
  },
  fadeInMusic: function(name) {
    if (!this.enabled) return
    this.playMusic(name, 0)
    this.playingMusic.fade(0, MUSIC_VOLUME, 2000, this.playingMusicId)
  },
  stopMusic() {
    if (!this.enabled) return
    this.playingMusic.fade(MUSIC_VOLUME, 0, 1000, this.playingMusicId)
  }
}
