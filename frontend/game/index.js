require('./sounds')
require('./multiplayer')

window.levels = {
  '1-1': require('./levels/tooploox_1-1'),
  '1-2': require('./levels/tooploox_1-2')
}
window.currentLevel = '1-1'
window.getLevel = function() {
  return window.levels[window.currentLevel]
}

const canvas = document.getElementById('foreground')
window.user = {
  username: canvas.getAttribute('data-username'),
  shortname: canvas.getAttribute('data-shortname'),
  animationOffset: parseInt(canvas.getAttribute('data-animationOffset'), 10)
}

window.developerTools = false
