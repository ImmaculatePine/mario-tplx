const io = require('socket.io-client')

const socket = io()

window.multiplayer = {
  players: {},
  currentPlayer: null,
  room: null,
  updatesIntervalId: null,
  connect: function(payload) {
    this.currentPlayer = payload.uuid
    this.room = payload.room
  },
  disconnect: function() {
    this.leaveRoom()
    this.currentPlayer = null
    if (this.updatesIntervalId) {
      clearInterval(this.updatesIntervalId)
      this.updatesIntervalId = null
    }
    socket.disconnect()
  },
  leaveRoom: function() {
    this.players = {}
    this.room = null
  },
  addPlayers: function(players) {
    for (let i = players.length - 1; i >= 0; i--) {
      this.addPlayer(players[i])
    }
  },
  addPlayer: function(player) {
    this.players[player.uuid] = {
      time: player.time,
      x: player.x,
      y: player.y,
      username: player.username,
      shortname: player.shortname,
      ghostAnimationOffset: player.ghostAnimationOffset || 0
    }
  },
  removePlayer: function(player) {
    delete this.players[player.uuid]
  },
  updatePlayer: function(payload) {
    const player = this.players[payload.uuid]
    if (player) {
      player.time = payload.time
      player.x = payload.x
      player.y = payload.y
    }
  }
}
window.socket = socket
