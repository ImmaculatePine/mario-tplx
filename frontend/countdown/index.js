import style from './style.scss'

import $ from 'jquery'
window.jQuery = $
require('flipclock/compiled/flipclock.js')
const FlipClock = window.FlipClock

$(document).ready(() => {
  const $clock = $('.clock')
  const timeout = parseInt($clock.data('timeout'), 10)
  if (timeout <= 0) {
    location.reload()
  } else {
    $clock.FlipClock(timeout, {
      clockFace: 'DailyCounter',
      countdown: true,
      callbacks: {
        stop: () => location.reload()
      }
    })
  }
})
