const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

let plugins = [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  new CopyWebpackPlugin([{ from: 'static' }, { from: 'frontend/game/engine' }]),
  new webpack.optimize.UglifyJsPlugin({
    compressor: {
      warnings: false,
    },
  })
]

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: {
    countdown: './frontend/countdown/index',
    login: './frontend/login/index',
    game: './frontend/game/index'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/static/'
  },
  plugins: plugins,
  node: {
    fs: 'empty',
  },
  module: {
    loaders: [
      {
        test: /frontend\/.*\.js$/,
        loader: 'babel-loader',
        include: __dirname,
        exclude: /node_modules/
      },
      {
        test: /\.(css|scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(jpg|jpeg|gif|png|ico)$/,
        loader: 'file-loader',
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
    ],
  },
}
